#!/usr/bin/env python3

from Collatz import max_cycle_length

def eval_region(start: int) -> int:
    return max_cycle_length(start, start + 1000)

print(list(map(eval_region, range(1, 1000001, 1000))))