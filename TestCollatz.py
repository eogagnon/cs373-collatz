#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eogagnon_1(self):
        # only catches one interval
        v = collatz_eval(9001, 9999)
        self.assertEqual(v, 260)

    def test_eogagnon_2(self):
        v = collatz_eval(10001, 10999)
        self.assertEqual(v, 268)

    def test_eogagnon_3(self):
        # only catches one interval
        v = collatz_eval(4001, 4999)
        self.assertEqual(v, 215)

    def test_eogagnon_4(self):
        # catch three intervals
        v = collatz_eval(9500, 10501)
        self.assertEqual(v, 255)

    def test_eogagnon_5(self):
        # catch three intervals
        v = collatz_eval(10500, 11501)
        self.assertEqual(v, 268)

    def test_eogagnon_6(self):
        # catch three intervals
        v = collatz_eval(7500, 8501)
        self.assertEqual(v, 252)

    def test_eogagnon_7(self):
        # catch two intervals
        v = collatz_eval(9000, 9001)
        self.assertEqual(v, 141)

    def test_eogagnon_8(self):
        # flip interval
        v = collatz_eval(9001, 9000)
        self.assertEqual(v, 141)

    def test_eogagnon_9(self):
        v = collatz_eval(9001, 1000)
        self.assertEqual(v, 262)

    def test_eogagnon_10(self):
        # catch several blocks
        v = collatz_eval(4444, 9999)
        self.assertEqual(v, 262)

    def test_eogagnon_11(self):
        # catch several blocks
        v = collatz_eval(12, 100000)
        self.assertEqual(v, 351)

    def test_eogagnon_12(self):
        # catch several blocks
        v = collatz_eval(100000, 9782)
        self.assertEqual(v, 351)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
